And why would you read me?

Makes Blindfolds Craftable without Blindsight Meme.

Why:
- Maybe you want to blindfold your Prisioner/Slaves because they're not meant to see something
- Maybe you just want to look stylish

Requirements:
- RimWorld - Ideology (DLC)

Special thanks to all the people that helped me on the RimWorld Discord, mainly those three wonderful persons: aelana, steveo.o and espioidsavant.
